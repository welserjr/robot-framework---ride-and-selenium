*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${url}              https://opensource-demo.orangehrmlive.com
@{CREDENTIALS}      Admin               admin123
&{LOGIN}            Username=Admin      Password=admin123

*** Test Cases ***
Test1
    Open Browser    ${url}              chrome
    Input Text      id=txtUsername      @{CREDENTIALS}[0]
    Input Text      id=txtPassword      &{LOGIN}[Password]
    Click Button    id=btnLogin
    Close Browser
    Log To Console  %{USERNAME} ran this test on %{os}
